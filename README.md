# Estudo de caso: #

Efetuar consulta por CEP na api utilizada pelo site dos correios, através do mesmo POST efetuado pelo formulário de consulta online.
Consultas inválidas devem ser ignoradas.

O retorno deve ser tratado adequadamente para os seguintes casos:

* Endereço encontrado;
* Endereço não encontrado;
* Pesquisa falhou ou o servidor está indisponível;

O endereço encontrado deve ser retornado através de uma estrutura.

### Dados do site dos correios em 29/08/2017 ###

Consulta:

* Endereço: http://www.buscacep.correios.com.br/sistemas/buscacep/resultadoBuscaCepEndereco.cfm
* Método: POST
* Content-type: application/x-www-form-urlencoded
* Parâmetro "relaxation": CEP a ser consultado
* Parâmetro "semelhante": Se a pesquisa será por semelhança ou não ("S" ou "N").

Retorno:

* Content-type: text/html;charset=ISO-8859-1
* Tag HTML com os resultados: <table class="tmptabela">