{
  Descri��o: Exemplo de consulta por CEP ao site dos correios.
  Autor: Julianno Jungle
  Data: 30/08/2017
}
unit Principal;

interface

uses
  Windows, Classes, Controls, Forms, StdCtrls, ConsultaCep;

type
  TForm1 = class(TForm)
    edtCep: TEdit;
    mmoRetorno: TMemo;
    Button1: TButton;
    Label1: TLabel;
    procedure Button1Click(Sender: TObject);
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
  lResultadoCorreios: TResultadoCorreios;
begin
  lResultadoCorreios := TConsultaCep.Pesquisar(edtCep.Text);

  mmoRetorno.Lines.Clear();
  mmoRetorno.Lines.Add('Logradouro: ' + lResultadoCorreios.Logradouro);
  mmoRetorno.Lines.Add('Bairro: ' + lResultadoCorreios.Bairro);
  mmoRetorno.Lines.Add('Cidade: ' + lResultadoCorreios.Cidade);
  mmoRetorno.Lines.Add('Cep: ' + lResultadoCorreios.Cep);
end;

end.
