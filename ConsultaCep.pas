{
  Descri��o: Classe de consulta por CEP ao site dos correios.
    Obs.: Esta classe � apenas um estudo e por isso a mesma � dependente do site de consulta
    dos correios. Caso o mencionado site seja alterado, esta classe precisa ser atualizada.
  Autor: Julianno F. C. Silva (http://linkedin.com/in/julianno)
  Data: 30/08/2017
}
unit ConsultaCep;

interface

uses
  SysUtils, Variants, Classes, Dialogs, StrUtils, IdHTTP, ActiveX, MSHTML;

type
  TResultadoCorreios = record
    Logradouro,
    Bairro,
    Cidade,
    Cep: String;
  end;

  TConsultaCep = class
  private
    function RetornarResultadoPorCep(pTabelaHtml, pCep: String): TResultadoCorreios;
    function ExtrairTabelaEnderecos(pDocumentoHtml: String): String;
    function EfetuarConsulta(pCep: ShortString): String;
  public
    class function Pesquisar(pCep: ShortString): TResultadoCorreios;
  end;

implementation

function TConsultaCep.RetornarResultadoPorCep(pTabelaHtml, pCep: String): TResultadoCorreios;
var
  lDocumentoHtml: IHTMLDocument2;
  lDocumentoTemporario: OleVariant;
  lElementos: IHTMLElementCollection;
  lTabela: IHTMLTable;
  lLinha: IHTMLTableRow;
  I: Integer;
begin
  try
    if (pTabelaHtml = '') or (pCep = '') or
      (not AnsiStartsText('<table', pTabelaHtml)) or
      (not AnsiEndsText('</table>', pTabelaHtml)) then
      Exit;

    // Cria um documento HTML completo, contendo somente a tabela no seu corpo.
    lDocumentoTemporario := VarArrayCreate([0,0], varVariant);
    lDocumentoTemporario[0] := pTabelaHtml;
    lDocumentoHtml := coHTMLDocument.Create as IHTMLDocument2;
    lDocumentoHtml.Write(PSafeArray(TVarData(lDocumentoTemporario).VArray));

    // Lista todas as tabelas e recupera a �nica inserida no documento criado.
    lElementos := lDocumentoHtml.all.tags('table') as IHTMLElementCollection;
    lTabela := lElementos.item(0, EmptyParam) as IHTMLTable;

    // Lista todas as linhas da tabela.
    lElementos := lTabela.rows as IHTMLElementCollection;

    // Ignora a primeira linha da tabela, pois � o cabe�alho.
    for I := 1 to lElementos.length - 1 do
    begin
      lLinha := lElementos.item(I, EmptyParam) as IHTMLTableRow;

      Result.Logradouro := Trim((lLinha.cells.item(0, EmptyParam) as IHTMLElement).innerText);
      Result.Bairro := Trim((lLinha.cells.item(1, EmptyParam) as IHTMLElement).innerText);
      Result.Cidade := Trim((lLinha.cells.item(2, EmptyParam) as IHTMLElement).innerText);
      Result.Cep := Trim((lLinha.cells.item(3, EmptyParam) as IHTMLElement).innerText);

      // Como o site dos correios lista mais de um resultado, filtramos apenas o que possuir o cep exato.
      if StringReplace(Result.Cep,'-','',[]) = StringReplace(pCep,'-','',[]) then
        Break;
    end;
  except
    Exit;
  end;
end;

function TConsultaCep.ExtrairTabelaEnderecos(pDocumentoHtml: String): String;
const
  cInicioTabela = '<table class="tmptabela">';
  cFimTabela = '</table>';
var
  lPosInicial,
  lPosFinal: Integer;
  lDocumentoHtmlParcial: String;
begin
  if (pDocumentoHtml = '') then
    Exit;

  lPosInicial := AnsiPos(cInicioTabela, pDocumentoHtml);

  if lPosInicial = 0 then
    Exit;

  lDocumentoHtmlParcial := Copy(pDocumentoHtml, lPosInicial, Length(pDocumentoHtml) - lPosInicial);
  lPosFinal := AnsiPos(cFimTabela, lDocumentoHtmlParcial);
  Result := Copy(lDocumentoHtmlParcial, 1, lPosFinal + Length(cFimTabela));
  Result := StringReplace(Result, #9, '', [rfReplaceAll]);
  Result := StringReplace(Result, #$D, '', [rfReplaceAll]);
end;

function TConsultaCep.EfetuarConsulta(pCep: ShortString): String;
var
  lConsulta: TIdHTTP;
  lUrl: String;
  lParametrosConsulta: TStringStream;
begin
  if (pCep = '') then
    Exit;

  lConsulta := TIdHTTP.Create(nil);
  try
    lConsulta.Request.ContentType := 'application/x-www-form-urlencoded';
    lUrl := 'http://www.buscacep.correios.com.br/sistemas/buscacep/resultadoBuscaCepEndereco.cfm';
    lParametrosConsulta := TStringStream.Create('relaxation=' + pCep + '&semelhante=N');
    Result := lConsulta.Post(lUrl, lParametrosConsulta);
  finally
    FreeAndNil(lConsulta);
  end;
end;

class function TConsultaCep.Pesquisar(pCep: ShortString): TResultadoCorreios;
var
  lConsultaCep: TConsultaCep;
  lCep,
  lTextoHtml: String;
  lCepNumerico: Int64;
begin
  lCep := StringReplace(pCep,'-','',[rfReplaceAll]);
  if (lCep = '') or (Length(lCep) <> 8) or (not TryStrToInt64(lCep, lCepNumerico)) then
    Exit;

  lConsultaCep := TConsultaCep.Create();
  try
    lTextoHtml := lConsultaCep.EfetuarConsulta(pCep);
    lTextoHtml := lConsultaCep.ExtrairTabelaEnderecos(lTextoHtml);
    Result := lConsultaCep.RetornarResultadoPorCep(lTextoHtml, pCep);
  finally
    FreeAndNil(lConsultaCep);
  end;
end;

end.
